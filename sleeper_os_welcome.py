import tkinter as tk
import webbrowser
import os

class WelcomeApp:
    def __init__(self, root):
        self.root = root
        root.title("Welcome to Sleeper OS")
        root.configure(bg="#FFFFFF")
        
        screen_width = root.winfo_screenwidth()
        screen_height = root.winfo_screenheight()

        window_width = int(screen_width * 1)
        window_height = int(screen_height * 1)

        x_position = (screen_width - window_width) // 2
        y_position = (screen_height - window_height) // 2

        root.geometry(f"{window_width}x{window_height}+{x_position}+{y_position}")

        # Load icon
        self.icon = self.load_image("/usr/local/bin/Sleeper-OS-Welcome/images/icon.png")
        if self.icon:
            root.iconphoto(False, self.icon)

        # Load and display logo
        self.logo = self.load_image("/usr/local/bin/Sleeper-OS-Welcome/images/logo.png")
        if self.logo:
            self.logo_label = tk.Label(root, image=self.logo, bg="#FFFFFF")
            self.logo_label.pack(pady=10)

        self.title_label = tk.Label(root, text="Welcome to Sleeper OS", font=("Ubuntu Regular", 16, "bold"), bg="#FFFFFF", fg="#000000")
        self.title_label.pack(pady=10)

        # Load and display website image
        self.website_image = self.load_image("/usr/local/bin/Sleeper-OS-Welcome/images/website_image.png")
        if self.website_image:
            self.website_label = tk.Label(root, image=self.website_image, bg="#FFFFFF")
            self.website_label.pack(pady=10)
            self.website_label.bind("<Button-1>", lambda event: self.open_website())

        # Load and display wiki image
        self.wiki_image = self.load_image("/usr/local/bin/Sleeper-OS-Welcome/images/wiki_image.png")
        if self.wiki_image:
            self.wiki_label = tk.Label(root, image=self.wiki_image, bg="#FFFFFF")
            self.wiki_label.pack(pady=10)
            self.wiki_label.bind("<Button-1>", lambda event: self.open_wiki())

        # Load and display mastodon image
        self.mastodon_image = self.load_image("/usr/local/bin/Sleeper-OS-Welcome/images/mastodon_image.png")
        if self.mastodon_image:
            self.mastodon_label = tk.Label(root, image=self.mastodon_image, bg="#FFFFFF")
            self.mastodon_label.pack(pady=10)
            self.mastodon_label.bind("<Button-1>", lambda event: self.open_mastodon())

        # Load and display install image
        self.install_image = self.load_image("/usr/local/bin/Sleeper-OS-Welcome/images/install_image.png")
        if self.install_image:
            self.install_label = tk.Label(root, image=self.install_image, bg="#FFFFFF")
            self.install_label.pack(pady=10)
            self.install_label.bind("<Button-1>", lambda event: self.run_installer())

    def load_image(self, file_path):
        try:
            image = tk.PhotoImage(file=file_path)
            print(f"Successfully loaded image: {file_path}")
            return image
        except Exception as e:
            print(f"Error loading image {file_path}: {e}")
            return None

    def open_website(self):
        webbrowser.open("https://sleeperos.sourceforge.io")

    def open_wiki(self):
        webbrowser.open("https://sleeperos.sourceforge.io/wiki")

    def open_mastodon(self):
        webbrowser.open("https://mastodon.social/@SleeperOS")

    def run_installer(self):
        os.system('sudo minstall')

if __name__ == "__main__":
    root = tk.Tk()
    app = WelcomeApp(root)
    root.mainloop()





